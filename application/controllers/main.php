<?php /**
* 
*/
class Main extends ci_controller
{
	public $data;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('traffic', 'tr', true);
		$this->load->model('sensor', 'sn', true);
		$this->tr->setField('sensor_traffics.id_traffic, sensor_traffics.id_sensor, sensor_traffics.capture_time, sensors.name, sensors.pin');
		$this->tr->activateJoin('sensors', 'sensor_traffics.id_sensor = sensors.id_sensor');
	}

	function index()
	{		
		$this->data["traffics"] = $this->tr->getData();
		$this->data["sensors"] = $this->sn->getData();
		$this->load->view('main', $this->data);
	}

	function orderBy()
	{
		$sort = $this->input->get('sort');
		$sensor = $this->input->get('sensor');
		$this->tr->setOrder('capture_time '.$sort);
		if($sensor!='all')
			$this->tr->setWhere(array('sensor_traffics.id_sensor' => $sensor));
		$this->data["traffics"] = $this->tr->getData();
		$this->data["sensors"] = $this->sn->getData();
		$this->load->view('main', $this->data);
	}

	function bersihkan()
	{
		$this->db->empty_table('sensor_traffics');
		$this->index();
	}
} ?>