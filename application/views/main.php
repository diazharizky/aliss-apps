<!DOCTYPE html>
<html>
	<head>
		<title>ALiSS Cube Webserver</title>	
<?php 
	echo link_tag('style/css/bootstrap.min.css', 'stylesheet', 'text/css');
?>
		<script src="<?php echo $this->config->item('style') ?>js/jquery-1.11.3.min.js"></script>
		<script src="<?php echo $this->config->item('style') ?>js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="col-md-8 col-md-offset-2">
				<h3>ALiSS Cube (A Little Security System)</h3>
				<br />
			<?php echo form_open(site_url('main/orderBy'), 'class="form-inline" method="get"') ?>
					<div class="form-group">
						<label for="pengurutan">Pengurutan:&nbsp;</label>
						<select class="form-control" name="sort" id="pengurutan" required="">
							<option value="">-- Pilih --</option>
							<option value="asc">Terlama ke terbaru</option>
							<option value="desc">Terbaru ke terlama</option>
						</select>
					</div>
					<div class="form-group">
						<label for="sensor">&nbsp;Pilih Sensor:&nbsp;</label>
						<select class="form-control" name="sensor" id="sensor">
							<option value="all">Semua sensor</option>
							<option value="1">PIR (Sensor Manusia)</option>
							<option value="2">Flame (Sensor Api)</option>
							<option value="3">Gas (Sensor Gas LPG)</option>
						</select>
					</div>
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-sort"></span>&nbsp;Urutkan</button>
				</form>
				<br />
				<table class="table table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Sensor</th>
							<th>Waktu</th>
						</tr>
					</thead>
					<tbody>
					<?php
						$i = 1;
						foreach ($traffics as $key) {
					?>
						<tr>
							<td><?php echo $i ?></td>
							<td><?php echo $key->name ?></td>
							<td><?php echo $key->capture_time ?></td>
						</tr>
					<?php						
							$i++;
						}
					?>
					</tbody>											
				</table>
				<a href="<?php echo site_url('main/bersihkan') ?>" class="btn btn-warning"><span class="glyphicon glyphicon-trash"></span>&nbsp;Bersihkan Data</a>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){

			});
		</script>
	</body>
</html>