# ALiSS (A Little Security System) Apps #

ALiSS Apps adalah sebuah aplikasi berbasis web yang digunakan untuk merekam aktivitas sensor pada ALiSS Cube. Aplikasi ini menggunakan bahasa pemrograman PHP dan menggunakan framework Codeigniter.

### Untuk apa aplikasi ini? ###

* Melihat data yang terekam oleh sensor
* Melihat history, mendukung fitur filter data
* Clean up data yang terekam oleh sensor agar tidak memenuhi memory

Pengembang: Diaz Harizky Firdaus